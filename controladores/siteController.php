<?php
namespace controladores;

class siteController extends Controller{
    private $miPie;
    private $miMenu;
        
    public function __construct() {
        parent::__construct();
        $this->miPie="Crea los puntos de interés en el mapa"; // muestra el pie de de la apliacacion
        $this->miMenu=[ // se envia los botones del menú
        "Inicio"=>$this->crearRuta(["accion"=>"index"]),
            "Hospitales"=>$this->crearRuta(["accion"=>"ejercicio1"]),
            "Restaurantes"=>$this->crearRuta(["accion"=>"ejercicio2"]),
            "Autobuses"=>$this->crearRuta(["accion"=>"ejercicio3"]),
            "Mapa"=>$this->crearRuta(["accion"=>"ejercicio3"]),
            
        ];
    }
    
    /*
     * Esta Acción envia el dibujo del menú de la aplicación
     */
    
    public function indexAccion(){ 
      $this->render([
          "vista"=>"index",
          "pie"=>$this->miPie,
          "menu"=>(new \clases\Menu($this->miMenu,"Inicio"))->html(),
          "menu1"=>(new \clases\Menu([
            "Hospitales"=>$this->crearRuta(["controlador"=>"Hospital","accion"=>"crear"]), //botones con parametros controlado=>'' accion=>''
            "Restaurantes"=>$this->crearRuta(["accion"=>"ejercicio2"]),
            "Autobuses"=>$this->crearRuta(["accion"=>"ejercicio3"]),
            "Dibujar"=>$this->crearRuta(["accion"=>"ejercicio4"]),
            "Mapa"=>$this->crearRuta(["accion"=>"ejercicio4"]),
          ],"Inicio"))
            ->setClass_a("uno")
             ->setClass_li("")
             ->setClass_ul("")
              ->setClass_nav("")
            ->html(),
    ]);
    }
    
    
}